# demo-environment

Ansible Even Driven Automation NetOps Demo

## Demo/Lab Developers:

*Rafael Minguillon*, EMEA Ansible Tecnical Account Manager, Red Hat

*Cesar Fernandez*, EMEA Ansible Specialist Solution Architect, Red Hat

## Prerequisites

- ServiceNow instance.
- An AWS Sandbox. The image tested to deploy the cloud routers is the AMI ami-029be4081d4f7c450. You NEED to authorize the AMI in the AWS marketplace BEFORE of its use.
- OpenShift Cluster +4.13 with admin rights.
- Ansible Navigator.
- Demo Environment project.

### ServiceNow

You can request ServiceNow Developer Instance in the following [link](https://developer.servicenow.com/).

### OpenShift

If you don't have an OpenShift Cluster 4.13 with admin rights, you can request one in the following [link](https://demo.redhat.com/catalog?category=Workshops&item=babylon-catalog-prod%2Fsandboxes-gpte.ocp413-wksp.prod).

### Ansible Navigator

- If you have Linux, you can install the `ansible-navigator` tool following the steps described in the following [link](https://ansible-navigator.readthedocs.io/installation/#linux).

> Note: Run the command `ansible-navigator --version` to make sure it was installed correctly.

- After installing it, create the `inventory` and the `ansible-navigator` definition file:

```sh
cd ~
mkdir ansible-navigator
cat << EOF > ansible-navigator/inventory
[controller]
localhost ansible_connection=local
EOF
cat << EOF > ansible-navigator/ansible-navigator.yml
---
ansible-navigator:
  ansible:
    inventory:
      entries:
      - ./inventory
  app: run
  editor:
    command: vim_from_setting
    console: false
  execution-environment:
    container-engine: podman
    image: quay.io/ansible_eda/ansible-eda-ee:2.0
    pull:
      policy: missing
  logging:
    append: true
    file: /tmp/navigator/ansible-navigator.log
    level: debug
  playbook-artifact:
    enable: false
EOF
```

## How to deploy the demo environment

### Demo Environment Project

- Clone the demo environment project:

```sh
cd ~
git clone https://gitlab.com/ansible-eda-netops-demo/demo-environment.git
```

### Deploy Demo Environment

- Make a copy of the vars template provided and rename as follows.

```
cd vars
cp template-demo-config.yml demo-config.yml
```
Provide the empty variables adding your own data.

- Deploy the demo environment:

```sh
cd ~/ansible-navigator
ansible-navigator run ../demo-environment/ocp-demo-deploy.yml -m stdout \
  -e 'ansible_python_interpreter=/usr/bin/python3' \
  -e 'openshift_token=<OPENSHIFT_API>' \
  -e 'openshit_storage_class=gp3-csi'
```
